const http = require('http');
const crearServerPrivado = (nombre ,puerto)=>{
    var servidor = http.createServer(function(req, res){
        res.setHeader('Content-type', 'text/html');
        res.end(`<h1>Bienvenido este es tu servidor con el nombre: ${nombre}, ubicado en el puerto: ${puerto}</h1>`);
    });
    servidor.listen(puerto);
}
module.exports = {
    crearServerPrivado
}