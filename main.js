const http = require('http');
const views = require('./views');

const handleServer = function(req, res){ 

    views.fileSystem(req, res);
}

var servidor = http.createServer(handleServer);

servidor.listen(8080, function(){
    console.log("Escuchando en el puerto 8080");
});